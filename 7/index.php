<?php
include "conn.php";
$query = mysqli_query($connection,"SELECT product_categories.id,product_categories.name,COUNT(products.category_id) as jumlah_product FROM products JOIN product_categories ON product_categories.id = products.category_id GROUP BY products.category_id");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Product</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="bootstrap.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-light">
		<a class="navbar-brand ml-5" href="index.php"><img src="img/logo-white-sm.png" width="50"> PRODUCT</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<!-- <a class="nav-link ml-5" href="index.php">Dashboard <span class="sr-only">(current)</span></a> -->
				</li>
			</ul>
		</div>
	</nav>
<div class="container-fluid"> 
	<div class="row"> 
		<div class="col-3"> 
			<div class="sidebar">
					 <a class="" href="index.php"><div class="menu text-center"><i class="fa fa-user"></i> Dashboard</div></a>
			</div>		
		</div>
		<div class="col-9">
			<div class="wrap">
				<h3>Product Quantity</h3>
				<div class="underline"></div>
				<div class="underline2"></div>
			<div class="card">
				<table class="table">
					<thead class="thead-light">
						<tr class="text-center">
							<th scope="col">ID</th>
							<th scope="col">Name</th>
							<th scope="col">Jumlah Product</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($query as $k) { ?>
						<tr class="text-center">
							<th scope="row"><?=$k['id']?></th>
							<td><?=$k['name']?></td>
							<td><?=$k['jumlah_product']?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		</div>
	</div>			
</div>
</body>
</html>