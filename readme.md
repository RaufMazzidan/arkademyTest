# REQRUITMENT TEST ARKADEMY

### (1) JSON RETURN VALUE
Click [here](1/Soal1.js) to get the file

Untuk menjalankan program, buka cmd ke route filenya kemudian
```bash
node Soal1.js
```
### (2) USERNAME VALIDATION

Click [here](2/Soal2.js) to get the file

Untuk menjalankan program, buka cmd ke route filenya kemudian
```bash
node Soal2.js
```
### (3) TRIANGLE NUMBER

Click [here](3/Soal3.js) to get the file

Untuk menjalankan program, buka cmd ke route filenya kemudian
```bash
node Soal3.js
```
### (4) HANDSHAKE COUNT
Click [here](4/Soal4.js) to get the file

Untuk menjalankan program, buka cmd ke route filenya kemudian
```bash
node Soal4.js
```

### (5) LETTER COUNT
Click [here](5/Soal5.js) to get the file

Untuk menjalankan program, buka cmd ke route filenya kemudian
```bash
node Soal5.js
```

### (6) QUERY
Click [here](6/) to get the folder
SQL File [here](6/ark_product.sql)
Query File [here](6/query.txt)
Untuk Mencoba query :
1. Buat Database "ark_product"
2. Kemudian import file SQL kedalam Database
3. Kemudian coba Query

![Screenshot](6/query.png)

### (7) SHOWING RESULT OF QUERY
Click [here](7/) to get the folder
SQL File [here](7/ark_product.sql)
Untuk Mencoba Aplikasi :
1. Buat Database dengan nama "ark_product"
2. Kemudian import file SQL kedalam Database
3. Copy Project PHP ke dalam htdocs di XAMPP
4. Buka Di Browser
 
![Screenshot](7/Web.png)
